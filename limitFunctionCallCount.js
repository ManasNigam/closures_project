
const limitFunctionCallCount = (cb, n) => {
    let callCount = 0;

    const fn = (arguments) => {
        if (callCount < n) {
            ++callCount;
            return cb(arguments);
        }
        return null;
    }
    return fn;
}


module.exports = limitFunctionCallCount