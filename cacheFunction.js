const cacheFunction = (cb) => {
    let cache = {};

    return fn = (...arguments) => {

        if (arguments in cache) {
            console.log("Cache called");

            return cache[arguments];
        }
        else {
            console.log("pushed in cache");
            cache[arguments] = cb(...arguments);
            return cache[arguments];
        }


    }
}


module.exports = cacheFunction