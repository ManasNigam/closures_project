const counterFactory = () => {
    const obj = {
        increment: (variable) => {
            return ++variable;
        },
        decrement: (variable) => {
            return --variable;
        }
    }
    return obj;
}

module.exports = counterFactory